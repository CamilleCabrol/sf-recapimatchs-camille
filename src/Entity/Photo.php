<?php

namespace App\Entity;

use App\Repository\PhotoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotoRepository::class)
 */
class Photo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legende;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chemin;

    /**
     * @ORM\ManyToOne(targetEntity=Gymnase::class, inversedBy="lesPhotos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leGymnase;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLegende(): ?string
    {
        return $this->legende;
    }

    public function setLegende(?string $legende): self
    {
        $this->legende = $legende;

        return $this;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getLeGymnase(): ?Gymnase
    {
        return $this->leGymnase;
    }

    public function setLeGymnase(?Gymnase $leGymnase): self
    {
        $this->leGymnase = $leGymnase;

        return $this;
    }
}
