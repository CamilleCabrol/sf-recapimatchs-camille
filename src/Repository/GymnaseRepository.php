<?php

namespace App\Repository;

use App\Entity\Gymnase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gymnase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gymnase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gymnase[]    findAll()
 * @method Gymnase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GymnaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gymnase::class);
    }

    // /**
    //  * @return Gymnase[] Returns an array of Gymnase objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Gymnase
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
