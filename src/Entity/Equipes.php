<?php

namespace App\Entity;

use App\Repository\EquipesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EquipesRepository::class)
 */
class Equipes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Gymnase::class, inversedBy="lesEquipes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leGymnase;

    /**
     * @ORM\OneToMany(targetEntity=Arbitres::class, mappedBy="lEquipe", orphanRemoval=true)
     */
    private $lesArbitres;

    /**
     * @ORM\OneToMany(targetEntity=Matchs::class, mappedBy="equipeDomicile", orphanRemoval=true)
     */
    private $lesMatchsDomicile;

    /**
     * @ORM\OneToMany(targetEntity=Matchs::class, mappedBy="equipeExterieur", orphanRemoval=true)
     */
    private $lesMatchsExterieur;

    public function __construct()
    {
        $this->lesArbitres = new ArrayCollection();
        $this->lesMatchsDomicile = new ArrayCollection();
        $this->lesMatchsExterieur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLeGymnase(): ?Gymnase
    {
        return $this->leGymnase;
    }

    public function setLeGymnase(?Gymnase $leGymnase): self
    {
        $this->leGymnase = $leGymnase;

        return $this;
    }

    /**
     * @return Collection|Arbitres[]
     */
    public function getLesArbitres(): Collection
    {
        return $this->lesArbitres;
    }

    public function addLesArbitre(Arbitres $lesArbitre): self
    {
        if (!$this->lesArbitres->contains($lesArbitre)) {
            $this->lesArbitres[] = $lesArbitre;
            $lesArbitre->setLEquipe($this);
        }

        return $this;
    }

    public function removeLesArbitre(Arbitres $lesArbitre): self
    {
        if ($this->lesArbitres->removeElement($lesArbitre)) {
            // set the owning side to null (unless already changed)
            if ($lesArbitre->getLEquipe() === $this) {
                $lesArbitre->setLEquipe(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Matchs[]
     */
    public function getLesMatchsDomicile(): Collection
    {
        return $this->lesMatchsDomicile;
    }

    public function addLesMatchsDomicile(Matchs $lesMatchsDomicile): self
    {
        if (!$this->lesMatchsDomicile->contains($lesMatchsDomicile)) {
            $this->lesMatchsDomicile[] = $lesMatchsDomicile;
            $lesMatchsDomicile->setEquipeDomicile($this);
        }

        return $this;
    }

    public function removeLesMatchsDomicile(Matchs $lesMatchsDomicile): self
    {
        if ($this->lesMatchsDomicile->removeElement($lesMatchsDomicile)) {
            // set the owning side to null (unless already changed)
            if ($lesMatchsDomicile->getEquipeDomicile() === $this) {
                $lesMatchsDomicile->setEquipeDomicile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Matchs[]
     */
    public function getLesMatchsExterieur(): Collection
    {
        return $this->lesMatchsExterieur;
    }

    public function addLesMatchsExterieur(Matchs $lesMatchsExterieur): self
    {
        if (!$this->lesMatchsExterieur->contains($lesMatchsExterieur)) {
            $this->lesMatchsExterieur[] = $lesMatchsExterieur;
            $lesMatchsExterieur->setEquipeExterieur($this);
        }

        return $this;
    }

    public function removeLesMatchsExterieur(Matchs $lesMatchsExterieur): self
    {
        if ($this->lesMatchsExterieur->removeElement($lesMatchsExterieur)) {
            // set the owning side to null (unless already changed)
            if ($lesMatchsExterieur->getEquipeExterieur() === $this) {
                $lesMatchsExterieur->setEquipeExterieur(null);
            }
        }

        return $this;
    }
}
