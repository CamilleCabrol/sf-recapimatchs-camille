<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Equipes;

class EquipesController extends AbstractController
{
    /**
     * @Route("/equipes", name="equipes")
     */
    public function index(): Response
    {
        /*
        * récupère toutes les equipes
        */
        
        // récupère le repository equipe
        $repoEquipe = $this->getDoctrine()->getRepository(Equipes::class);
        
        // récupère les equipes
        $lesEquipes = $repoEquipe->findAll();
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_equipes' => $lesEquipes
        );
        
        // render la page equipes/index
        return $this->render('equipes/index.html.twig', $params);
    }
    
    /**
    * @Route("/equipe/{id}", name="equipe_show")
    */
    public function show($id){
        
        /*
        * récupère l'équipe correspondant à l'id
        */
        
        // récupère le repository équipe
        $repoEquipe = $this->getDoctrine()->getRepository(Equipes::class);
        
        // récupère l'équipe
        $uneEquipe = $repoEquipe->find($id);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'une_equipe' => $uneEquipe
        );
        
        // render la page equipe/index
        return $this->render('equipes/show.html.twig', $params);
    }
    
    /**
    *
    * @Route("/equipe/search/{$idMatchs}", name="equipes_matchs")
    */
    public function showMatchs($idMatchs)
    {
        /*
        * récupère toutes les equipes
        */
        
        // récupère le repository equipes
        $repoEquipe = $this->getDoctrine()->getRepository(Equipes::class);
        
        // récupère l'équipe
        $lesEquipes = $repoEquipe->findAllByMatchs1($idMatchs);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_equipes' => $lesEquipes
        );
        
        // render la page equipe/index
        return $this->render('equipes/index.html.twig', $params);
    }
}
