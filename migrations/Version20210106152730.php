<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210106152730 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE arbitres ADD CONSTRAINT FK_B12431B2F2D967F1 FOREIGN KEY (l_equipe_id) REFERENCES equipes (id)');
        $this->addSql('CREATE INDEX IDX_B12431B2F2D967F1 ON arbitres (l_equipe_id)');
        $this->addSql('ALTER TABLE equipes ADD le_gymnase_id INT NOT NULL');
        $this->addSql('ALTER TABLE equipes ADD CONSTRAINT FK_76F7625AD7F4A27B FOREIGN KEY (le_gymnase_id) REFERENCES gymnase (id)');
        $this->addSql('CREATE INDEX IDX_76F7625AD7F4A27B ON equipes (le_gymnase_id)');
        $this->addSql('ALTER TABLE matchs ADD equipe_domicile_id INT NOT NULL, ADD equipe_exterieur_id INT NOT NULL, ADD arbitre_principal_id INT NOT NULL, ADD arbitre_secondaire_id INT NOT NULL');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E60415FE1AEAD FOREIGN KEY (equipe_domicile_id) REFERENCES equipes (id)');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E604121ECD755 FOREIGN KEY (equipe_exterieur_id) REFERENCES equipes (id)');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E6041614A633 FOREIGN KEY (arbitre_principal_id) REFERENCES arbitres (id)');
        $this->addSql('ALTER TABLE matchs ADD CONSTRAINT FK_6B1E60417D9255A3 FOREIGN KEY (arbitre_secondaire_id) REFERENCES arbitres (id)');
        $this->addSql('CREATE INDEX IDX_6B1E60415FE1AEAD ON matchs (equipe_domicile_id)');
        $this->addSql('CREATE INDEX IDX_6B1E604121ECD755 ON matchs (equipe_exterieur_id)');
        $this->addSql('CREATE INDEX IDX_6B1E6041614A633 ON matchs (arbitre_principal_id)');
        $this->addSql('CREATE INDEX IDX_6B1E60417D9255A3 ON matchs (arbitre_secondaire_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE arbitres DROP FOREIGN KEY FK_B12431B2F2D967F1');
        $this->addSql('DROP INDEX IDX_B12431B2F2D967F1 ON arbitres');
        $this->addSql('ALTER TABLE equipes DROP FOREIGN KEY FK_76F7625AD7F4A27B');
        $this->addSql('DROP INDEX IDX_76F7625AD7F4A27B ON equipes');
        $this->addSql('ALTER TABLE equipes DROP le_gymnase_id');
        $this->addSql('ALTER TABLE matchs DROP FOREIGN KEY FK_6B1E60415FE1AEAD');
        $this->addSql('ALTER TABLE matchs DROP FOREIGN KEY FK_6B1E604121ECD755');
        $this->addSql('ALTER TABLE matchs DROP FOREIGN KEY FK_6B1E6041614A633');
        $this->addSql('ALTER TABLE matchs DROP FOREIGN KEY FK_6B1E60417D9255A3');
        $this->addSql('DROP INDEX IDX_6B1E60415FE1AEAD ON matchs');
        $this->addSql('DROP INDEX IDX_6B1E604121ECD755 ON matchs');
        $this->addSql('DROP INDEX IDX_6B1E6041614A633 ON matchs');
        $this->addSql('DROP INDEX IDX_6B1E60417D9255A3 ON matchs');
        $this->addSql('ALTER TABLE matchs DROP equipe_domicile_id, DROP equipe_exterieur_id, DROP arbitre_principal_id, DROP arbitre_secondaire_id');
    }
}
