<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Arbitres;
use App\Entity\Equipes;
use App\Entity\Gymnase;
use App\Entity\Matchs;
use App\Entity\Photo;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        /*
        * pas besoin d'instancier un objet de type EntityManager
        * le squelette proposé injecte une dépendance d'ObjectManager
        */
        
        // instancie un objet générateur faker
        $faker = \Faker\Factory::create('fr-FR');
        
        // boucle de création Arbitre
        for($i = 0; $i < 10; $i++){
            
            // instancie un nouvel arbitre
            $unArbitre = new Arbitres();
            
            // génère des données aléatoires
            $nom = $faker->lastName;
            $prenom = $faker->firstName;
            $tel = $faker->phoneNumber;
            
            $unArbitre->setNom($nom)
                ->setPrenom($prenom)
                ->setTel($tel);
                
            // enregistre l'arbitre dans l'objet manager
            $manager->persist($unArbitre);
        }
        
        // boucle de création Equipe
        for($i = 0; $i < 6; $i++){
            
            // instancie une nouvelle equipe
            $uneEquipe = new Equipes();
            
            // génère des données aléatoires
            $nom = $faker->userName;
            
            $uneEquipe->setNom($nom);
                
            // enregistre l'équipe dans l'objet manager
            $manager->persist($uneEquipe);
        }
        
        //// boucle de création Matchs
        //for($i = 0; $i < 30; $i++){
            
            //// instancie un nouveau matchs
            //$unMatchs = new Matchs();
            
            //// génère des données aléatoires
            //$date = $faker->date('Y-m-d');
            
            //$unMatchs->setDate($date);
                
            //// enregistre le matchs dans l'objet manager
            //$manager->persist($unMatchs);
        //}
        
        // boucle de création Gymnase
        for($i = 0; $i < 6; $i++){
            
            // instancie une nouvelle gymnase
            $unGymnase = new Gymnase();
            
            // génère des données aléatoires
            $ville = $faker->city;
            $rue = $faker->streetAddress;
            $cp = $faker->postcode;
            
            $unGymnase->setVille($ville)
                    ->setRue($rue)
                    ->setCp($cp);
                
            // enregistre le gymnase dans l'objet manager
            $manager->persist($unGymnase);
        }
        
        // met à jour la base de données
        $manager->flush();
        
        // $product = new Product();
        // $manager->persist($product);
        
        $manager->flush();
    }
}
