<?php

namespace App\Entity;

use App\Repository\ArbitresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArbitresRepository::class)
 */
class Arbitres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @ORM\ManyToOne(targetEntity=Equipes::class, inversedBy="lesArbitres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lEquipe;

    /**
     * @ORM\OneToMany(targetEntity=Matchs::class, mappedBy="arbitrePrincipal", orphanRemoval=true)
     */
    private $lesMatchsArbitresPpal;

    /**
     * @ORM\OneToMany(targetEntity=Matchs::class, mappedBy="arbitreSecondaire", orphanRemoval=true)
     */
    private $lesMatchsArbitresScndaire;

    public function __construct()
    {
        $this->lesMatchsArbitresPpal = new ArrayCollection();
        $this->lesMatchsArbitresScndaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getLEquipe(): ?Equipes
    {
        return $this->lEquipe;
    }

    public function setLEquipe(?Equipes $lEquipe): self
    {
        $this->lEquipe = $lEquipe;

        return $this;
    }

    /**
     * @return Collection|Matchs[]
     */
    public function getLesMatchsArbitresPpal(): Collection
    {
        return $this->lesMatchsArbitresPpal;
    }

    public function addLesMatchsArbitresPpal(Matchs $lesMatchsArbitresPpal): self
    {
        if (!$this->lesMatchsArbitresPpal->contains($lesMatchsArbitresPpal)) {
            $this->lesMatchsArbitresPpal[] = $lesMatchsArbitresPpal;
            $lesMatchsArbitresPpal->setArbitrePrincipal($this);
        }

        return $this;
    }

    public function removeLesMatchsArbitresPpal(Matchs $lesMatchsArbitresPpal): self
    {
        if ($this->lesMatchsArbitresPpal->removeElement($lesMatchsArbitresPpal)) {
            // set the owning side to null (unless already changed)
            if ($lesMatchsArbitresPpal->getArbitrePrincipal() === $this) {
                $lesMatchsArbitresPpal->setArbitrePrincipal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Matchs[]
     */
    public function getLesMatchsArbitresScndaire(): Collection
    {
        return $this->lesMatchsArbitresScndaire;
    }

    public function addLesMatchsArbitresScndaire(Matchs $lesMatchsArbitresScndaire): self
    {
        if (!$this->lesMatchsArbitresScndaire->contains($lesMatchsArbitresScndaire)) {
            $this->lesMatchsArbitresScndaire[] = $lesMatchsArbitresScndaire;
            $lesMatchsArbitresScndaire->setArbitreSecondaire($this);
        }

        return $this;
    }

    public function removeLesMatchsArbitresScndaire(Matchs $lesMatchsArbitresScndaire): self
    {
        if ($this->lesMatchsArbitresScndaire->removeElement($lesMatchsArbitresScndaire)) {
            // set the owning side to null (unless already changed)
            if ($lesMatchsArbitresScndaire->getArbitreSecondaire() === $this) {
                $lesMatchsArbitresScndaire->setArbitreSecondaire(null);
            }
        }

        return $this;
    }
}
