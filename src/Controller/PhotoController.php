<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Photo;

class PhotoController extends AbstractController
{
    /**
     * @Route("/photo", name="photo")
     */
    public function index(): Response
    {
        /*
        * récupère toutes les photos
        */
        
        // récupère le repository photo
        $repoPhoto = $this->getDoctrine()->getRepository(Photo::class);
        
        // récupère les photos
        $lesPhotos = $repoPhoto->findAll();
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_photos' => $lesPhotos
        );
        
        // render la page photo/index
        return $this->render('photo/index.html.twig', $params);
    }
    
    /**
    * @Route("/photo/{id}", name="photo_show")
    */
    public function show($id){
        
        /*
        * récupère la photo correspondant à l'id
        */
        
        // récupère le repository photo
        $repoPhoto = $this->getDoctrine()->getRepository(Photo::class);
        
        // récupère la photo
        $unePhoto = $repoPhoto->find($id);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'une_photo' => $unePhoto
        );
        
        // render la page photo/index
        return $this->render('photo/show.html.twig', $params);
    }
    
    /**
    *
    * @Route("/photo/search/{$idGymnase}", name="photo_gymnase")
    */
    public function showGymnase($idGymnase)
    {
        /*
        * récupère toutes les photos
        */
        
        // récupère le repository photo
        $repoPhoto = $this->getDoctrine()->getRepository(Photo::class);
        
        // récupère les photos
        $lesPhotos = $repoPhoto->findAllByGymnase($idGymnase);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_photos' => $lesPhotos
        );
        
        // render la page photo/index
        return $this->render('photo/index.html.twig', $params);
    }
}
