<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210106143652 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE arbitre (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipe (id INT AUTO_INCREMENT NOT NULL, le_gymnase_id INT NOT NULL, INDEX IDX_2449BA15D7F4A27B (le_gymnase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE equipe ADD CONSTRAINT FK_2449BA15D7F4A27B FOREIGN KEY (le_gymnase_id) REFERENCES gymnase (id)');
        $this->addSql('ALTER TABLE photo ADD le_gymnase_id INT NOT NULL');
        $this->addSql('ALTER TABLE photo ADD CONSTRAINT FK_14B78418D7F4A27B FOREIGN KEY (le_gymnase_id) REFERENCES gymnase (id)');
        $this->addSql('CREATE INDEX IDX_14B78418D7F4A27B ON photo (le_gymnase_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE arbitre');
        $this->addSql('DROP TABLE equipe');
        $this->addSql('ALTER TABLE photo DROP FOREIGN KEY FK_14B78418D7F4A27B');
        $this->addSql('DROP INDEX IDX_14B78418D7F4A27B ON photo');
        $this->addSql('ALTER TABLE photo DROP le_gymnase_id');
    }
}
