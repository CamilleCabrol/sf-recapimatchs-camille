<?php

// src/Controller/AccueilController.php

/**
 * Description of AccueilController
 * 
 * @author Camille
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccueilController extends AbstractController {
    
    /**
     * Action Hello world !
     * @return Response
     */
    public function Hello1(){
        $html='<html><body>Hello world !</body></html>';
        return new Response($html);
    }
    
    /**
     * Action Hello world !
     * @return Response
     * @Route("/accueil/hello2")
     */
    public function Hello2(){
        $html='<html><body>Hello world le retour !</body></html>';
        return new Response($html);
    }
    
    /**
     * @Route("/accueil/bienvenue")
     */
    public function Bienvenue(){
        // récupère le pseudo du membre authentifié
        $pseudo = 'Camille';
        $nb_matchs = 3;
        $mes_matchs = array(
            'Perpignan',
            'Marseille',
            'Toulouse'
        );
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => $pseudo,
            'nb_matchs' => $nb_matchs,
            'mes_matchs' => $mes_matchs
        );
        
        // render la page Bienvenue
        return $this->render('accueil.html.twig', $params);
    }
    
    /**
    *
    * @return type
    * @Route("/", name="accueil")
    */
    public function Accueil(){
        // récupère le membre authentifié
        $pseudo = '';
        $nb_matchs = 8;
        $mes_matchs = array(
            'Perpignan',
            'Marseille',
            'Toulouse'
        );
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => $pseudo,
            'nb_matchs' => $nb_matchs,
            'mes_matchs' => $mes_matchs
        );
        
        // render la page accueil
            return $this->render('accueil.html.twig', $params);
    }
}
