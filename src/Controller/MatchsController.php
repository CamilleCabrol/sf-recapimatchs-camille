<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Matchs;

class MatchsController extends AbstractController
{
    /**
     * @Route("/matchs", name="matchs")
     */
    public function index(): Response
    {
        /*
        * récupère tous les matchs
        */
        
        // récupère le repository matchs
        $repoMatchs = $this->getDoctrine()->getRepository(Matchs::class);
        
        // récupère les matchs
        $lesMatchs = $repoMatchs->findAll();
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_matchs' => $lesMatchs
        );
        
        // render la page matchs/index
        return $this->render('matchs/index.html.twig', $params);
    }
    
    /**
    * @Route("/matchs/{id}", name="matchs_show")
    */
    public function show($id){
        
        /*
        * récupère le matchs correspondant à l'id
        */
        
        // récupère le repository matchs
        $repoMatchs = $this->getDoctrine()->getRepository(Matchs::class);
        
        // récupère le match
        $unMatchs = $repoMatchs->find($id);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'un_matchs' => $unMatchs
        );
        
        // render la page matchs/index
        return $this->render('matchs/show.html.twig', $params);
    }
    
    /**
    *
    * @Route("/matchs/search/{$idArbitre}", name="matchs_arbitre")
    */
    public function showArbitres($idArbitre)
    {
        /*
        * récupère tous les matchs
        */
        
        // récupère le repository matchs
        $repoMatchs = $this->getDoctrine()->getRepository(Matchs::class);
        
        // récupère les matchs
        $lesMatchs = $repoMatchs->findAllByArbitre($idArbitre);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_matchs' => $lesMatchs
        );
        
        // render la page matchs/index
        return $this->render('matchs/index.html.twig', $params);
    }
    
    /**
    *
    * @Route("/matchs/search/{$idEquipe}", name="matchs_equipe")
    */
    public function showEquipes($idEquipe)
    {
        /*
        * récupère tous les matchs
        */
        
        // récupère le repository matchs
        $repoMatchs = $this->getDoctrine()->getRepository(Matchs::class);
        
        // récupère les matchs
        $lesMatchs = $repoMatchs->findAllByArbitre($idEquipe);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_matchs' => $lesMatchs
        );
        
        // render la page matchs/index
        return $this->render('matchs/index.html.twig', $params);
    }
}
