<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Arbitres;
    
class ArbitresController extends AbstractController
{
    /**
     * @Route("/arbitres", name="arbitres")
     */
    public function index(): Response
    {
        /*
        * récupère tous les arbitres
        */
        
        // récupère le repository arbitre
        $repoArbitre = $this->getDoctrine()->getRepository(Arbitres::class);
        
        // récupère les arbitres
        $lesArbitres = $repoArbitre->findAll();
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_arbitres' => $lesArbitres
        );
        
        // render la page arbitres/index
        return $this->render('arbitres/index.html.twig', $params);
    }
    
    /**
    * @Route("/arbitre/{id}", name="arbitre_show")
    */
    public function show($id){
        
        /*
        * récupère l'arbitre correspondant à l'id
        */
        
        // récupère le repository arbitre
        $repoArbitre = $this->getDoctrine()->getRepository(Arbitres::class);
        
        // récupère l'arbitre
        $unArbitre = $repoArbitre->find($id);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'un_arbitre' => $unArbitre
        );
        
        // render la page arbitre/index
        return $this->render('arbitres/show.html.twig', $params);
    }
    
    /**
    *
    * @Route("/arbitre/search/{$idMatchs}", name="arbitre_matchs")
    */
    public function showMatchs($idMatchs)
    {
        /*
        * récupère tous les arbitres
        */
        
        // récupère le repository arbitres
        $repoArbitre = $this->getDoctrine()->getRepository(Arbitres::class);
        
        // récupère l'arbitre
        $lesArbitres = $repoArbitre->findAllByMatchs($idMatchs);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_arbitres' => $lesArbitres
        );
        
        // render la page arbitre/index
        return $this->render('arbitres/index.html.twig', $params);
    }
}
