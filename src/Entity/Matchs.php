<?php

namespace App\Entity;

use App\Repository\MatchsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MatchsRepository::class)
 */
class Matchs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Equipes::class, inversedBy="lesMatchsDomicile")
     * @ORM\JoinColumn(nullable=false)
     */
    private $equipeDomicile;

    /**
     * @ORM\ManyToOne(targetEntity=Equipes::class, inversedBy="lesMatchsExterieur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $equipeExterieur;

    /**
     * @ORM\ManyToOne(targetEntity=Arbitres::class, inversedBy="lesMatchsArbitresPpal")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arbitrePrincipal;

    /**
     * @ORM\ManyToOne(targetEntity=Arbitres::class, inversedBy="lesMatchsArbitresScndaire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arbitreSecondaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEquipeDomicile(): ?Equipes
    {
        return $this->equipeDomicile;
    }

    public function setEquipeDomicile(?Equipes $equipeDomicile): self
    {
        $this->equipeDomicile = $equipeDomicile;

        return $this;
    }

    public function getEquipeExterieur(): ?Equipes
    {
        return $this->equipeExterieur;
    }

    public function setEquipeExterieur(?Equipes $equipeExterieur): self
    {
        $this->equipeExterieur = $equipeExterieur;

        return $this;
    }

    public function getArbitrePrincipal(): ?Arbitres
    {
        return $this->arbitrePrincipal;
    }

    public function setArbitrePrincipal(?Arbitres $arbitrePrincipal): self
    {
        $this->arbitrePrincipal = $arbitrePrincipal;

        return $this;
    }

    public function getArbitreSecondaire(): ?Arbitres
    {
        return $this->arbitreSecondaire;
    }

    public function setArbitreSecondaire(?Arbitres $arbitreSecondaire): self
    {
        $this->arbitreSecondaire = $arbitreSecondaire;

        return $this;
    }
}
