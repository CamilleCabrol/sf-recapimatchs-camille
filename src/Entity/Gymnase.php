<?php

namespace App\Entity;

use App\Repository\GymnaseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GymnaseRepository::class)
 */
class Gymnase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="leGymnase", orphanRemoval=true)
     */
    private $lesPhotos;

    /**
     * @ORM\OneToMany(targetEntity=Equipes::class, mappedBy="leGymnase", orphanRemoval=true)
     */
    private $lesEquipes;

    public function __construct()
    {
        $this->lesPhotos = new ArrayCollection();
        $this->lesEquipes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(?string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getLesPhotos(): Collection
    {
        return $this->lesPhotos;
    }

    public function addLesPhoto(Photo $lesPhoto): self
    {
        if (!$this->lesPhotos->contains($lesPhoto)) {
            $this->lesPhotos[] = $lesPhoto;
            $lesPhoto->setLeGymnase($this);
        }

        return $this;
    }

    public function removeLesPhoto(Photo $lesPhoto): self
    {
        if ($this->lesPhotos->removeElement($lesPhoto)) {
            // set the owning side to null (unless already changed)
            if ($lesPhoto->getLeGymnase() === $this) {
                $lesPhoto->setLeGymnase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Equipes[]
     */
    public function getLesEquipes(): Collection
    {
        return $this->lesEquipes;
    }

    public function addLesEquipe(Equipes $lesEquipe): self
    {
        if (!$this->lesEquipes->contains($lesEquipe)) {
            $this->lesEquipes[] = $lesEquipe;
            $lesEquipe->setLeGymnase($this);
        }

        return $this;
    }

    public function removeLesEquipe(Equipes $lesEquipe): self
    {
        if ($this->lesEquipes->removeElement($lesEquipe)) {
            // set the owning side to null (unless already changed)
            if ($lesEquipe->getLeGymnase() === $this) {
                $lesEquipe->setLeGymnase(null);
            }
        }

        return $this;
    }

}
