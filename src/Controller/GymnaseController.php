<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Gymnase;

class GymnaseController extends AbstractController
{
    /**
     * @Route("/gymnase", name="gymnase")
     */
    public function index(): Response
    {
        /*
        * récupère tous les gymnases
        */
        
        // récupère le repository gymnase
        $repoGymnase = $this->getDoctrine()->getRepository(Gymnase::class);
        
        // récupère les gymnases
        $lesGymnases = $repoGymnase->findAll();
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'les_gymnases' => $lesGymnases
        );
        
        // render la page gymnase/index
        return $this->render('gymnase/index.html.twig', $params);
    }
    
    /**
    * @Route("/gymnase/{id}", name="gymnase_show")
    */
    public function show($id){
        
        /*
        * récupère le gymnase correspondant à l'id
        */
        
        // récupère le repository gymnase
        $repoGymnase = $this->getDoctrine()->getRepository(Gymnase::class);
        
        // récupère le gymnase
        $unGymnase = $repoGymnase->find($id);
        
        // paramètres pour la vue
        $params = array(
            'pseudo' => 'Camille',
            'un_gymnase' => $unGymnase
        );
        
        // render la page gymnase/index
        return $this->render('gymnase/show.html.twig', $params);
    }
}
